import React, { Component, PropTypes } from 'react';
import { NavItem } from 'react-bootstrap';

export default class Release extends Component {
  render() {
    return (
      <NavItem href="#">{this.props.release.milestone}</NavItem>
    );
  }
}

Release.propTypes = {
  release: PropTypes.object.isRequired,
};
