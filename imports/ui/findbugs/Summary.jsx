import React, { Component, PropTypes } from 'react';

export default class Summary extends Component {
  render() {
    return (
      <tr>
        <td>{this.props.summary.name}</td>
        <td>{this.props.summary.high}</td>
        <td>{this.props.summary.medium}</td>
      </tr>
    );
  }
}

Summary.propTypes = {
  summary: PropTypes.object.isRequired,
};
